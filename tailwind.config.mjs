import { fontFamily } from 'tailwindcss/defaultTheme'

/** @type {import('tailwindcss').Config} */
export default {
    content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
    theme: {
        extend: {
            colors: ({ colors }) => {
                return { light: colors.white, dark: colors.black }
            },
            fontFamily: {
                sans: ['"M PLUS 1 Variable"', ...fontFamily.sans],
            },
            screens: { xs: '425px' },
        },
    },
    plugins: [],
}
