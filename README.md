# **Personal Website**

Built with [Astro](https://astro.build), Partly inspired by [PilcrowOnPaper](https://github.com/pilcrowOnPaper)

_Visit [here](https://sxnic.dev)_

### ⚠️ **Note**

I still don't speak Japanese very well, and although I'm working on improving my skills, some sentences may seem unnatural to some. Please do not hesitate to report any inconsistencies and typos so that I can learn from my mistakes.
**Thanks!**
