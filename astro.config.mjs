import { defineConfig } from 'astro/config'

import tailwind from '@astrojs/tailwind'
import icon from 'astro-icon'

// https://astro.build/config
export default defineConfig({
    integrations: [tailwind(), icon()],

    // Output
    output: 'static',
    prefetch: true,

    // i18n
    i18n: {
        defaultLocale: 'en',
        locales: ['en', 'nl', 'ja'],
    },
})
