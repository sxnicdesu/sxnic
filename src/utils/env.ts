// Environment variables
export const EMAIL = import.meta.env.PUBLIC_EMAIL as string
export const DISCORD = import.meta.env.PUBLIC_DISCORD as string
export const TELEGRAM = import.meta.env.PUBLIC_TELEGRAM as string

export const GITHUB_URL = import.meta.env.PUBLIC_GITHUB_URL as string
export const GITLAB_URL = import.meta.env.PUBLIC_GITLAB_URL as string
