import { z } from 'zod'

import { GITHUB_URL, GITLAB_URL } from '@/utils/env'

import ProjectsJson from '@/utils/projects.json'

// REF: 1 = Base | 2 = Github | 3 = Gitlab
const schema = z.array(
    z
        .object({
            name: z.string().min(1, 'Required'),
            description: z.record(z.string().nullish()),
            url: z.string(),
            type: z.union([z.literal(1), z.literal(2), z.literal(3)]),
        })
        .transform((x) => {
            return {
                ...x,
                url:
                    x.type === 2
                        ? `${GITHUB_URL}/${x.url}`
                        : x.type === 3
                          ? `${GITLAB_URL}/${x.url}`
                          : x.url,
            }
        }),
)

export async function getProjects() {
    return await schema.parseAsync(ProjectsJson)
}
